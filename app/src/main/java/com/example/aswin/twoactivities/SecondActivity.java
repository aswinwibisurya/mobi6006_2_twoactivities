package com.example.aswin.twoactivities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_REPLY = 1;

    TextView tvReceivedMessage;
    EditText txtReplyMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tvReceivedMessage = findViewById(R.id.tvReceivedMessage);
        txtReplyMessage = findViewById(R.id.txtReplyMessage);

        Intent intent = getIntent();
        String message = intent.getStringExtra("message");

        tvReceivedMessage.setText(message);
    }

    public void reply(View view) {
        String replyMessage = txtReplyMessage.getText().toString();
        Intent data = new Intent();
        data.putExtra("replyMessage", replyMessage);

        setResult(Activity.RESULT_OK, data);
        finish();
    }
}
