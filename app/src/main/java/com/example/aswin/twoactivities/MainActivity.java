package com.example.aswin.twoactivities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText txtMessage;
    TextView tvReplyHeader;
    TextView tvReplyMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtMessage = findViewById(R.id.txtMessage);
        tvReplyHeader = findViewById(R.id.tvReplyHeader);
        tvReplyMessage = findViewById(R.id.tvReplyMessage);
    }

    public void sendMessage(View view) {
        String message = txtMessage.getText().toString();

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("message", message);

//        start activity without expecting a result
//        startActivity(intent);

//        expecting a result
        startActivityForResult(intent, SecondActivity.REQUEST_CODE_REPLY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SecondActivity.REQUEST_CODE_REPLY) {
            if(resultCode == Activity.RESULT_OK) {
                String replyMessage = data.getStringExtra("replyMessage");

                tvReplyMessage.setText(replyMessage);

                tvReplyHeader.setVisibility(View.VISIBLE);
                tvReplyMessage.setVisibility(View.VISIBLE);
            }
        }
    }
}
